﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using nine.ViewModels;

namespace nine
{
	public class ErrorResult : IHttpActionResult
	{
		APIResponse _error;
		HttpRequestMessage _request;

		public ErrorResult(APIResponse error, HttpRequestMessage request)
		{
			_error = error;
			_request = request;
		}
		public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
		{
			var response = new HttpResponseMessage()
			{
				Content = new ObjectContent<APIResponse>(_error, new JsonMediaTypeFormatter()),
				RequestMessage = _request,
				StatusCode = System.Net.HttpStatusCode.BadRequest
			};
			return Task.FromResult(response);
		}
	}
}