﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using nine.ViewModels;
using nineService;
using nineService.Models;

namespace nine.Controllers
{
	[Route()]
	public class PayloadController : ApiController
	{
		private ShowService _service;
		public PayloadController()
		{
			_service = new ShowService();
		}
		public PayloadController(ShowService service )
		{
			this._service = service;
		}

		[HttpGet]
		public IHttpActionResult Get()
		{
			return new ErrorResult(new APIResponse { error = "GET is not supported.  Try POST instead" }, Request);
		}

		[HttpPost]
		public async Task<IHttpActionResult> Post(HttpRequestMessage request)
		{
			var err = new APIResponse { error = "Could not decode request: JSON parsing failed" };
			var PRstr = await request.Content.ReadAsStringAsync();
			if (string.IsNullOrEmpty(PRstr))
			{
				return new ErrorResult(err, Request);
			}

			var response = _service.Process(PRstr);
			if (response.Errors.Count() > 0)
			{
				return new ErrorResult(err, Request);
			}

			var ret = new ViewModels.ShowResponseVM();
			ret.Response = response.Shows.Select(p => new ShowVM(p)).ToArray();

			return Ok(ret);
		}
	}
}
