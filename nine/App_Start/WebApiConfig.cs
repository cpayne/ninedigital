﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace nine
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			// Can't tell if I need to do this or its a trick question.
			// To play it safe, I've added.  Source:  https://stackoverflow.com/a/29914360
			config.Formatters.Insert(0, new TextMediaTypeFormatter());

			// Web API configuration and services

			// Web API routes
			config.MapHttpAttributeRoutes();
			
		}
	}
}
