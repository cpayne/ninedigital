﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using nineService.Models;

namespace nine.ViewModels
{
	public class ShowVM
	{
		public string Image { get; set; }
		public string Slug { get; set; }
		public string Title { get; set; }

		public ShowVM()
		{

		}
		public ShowVM(Show s)
		{
			if (s.Image != null && !string.IsNullOrEmpty(s.Image.ShowImage))
			{
				this.Image = s.Image.ShowImage;
			}

			this.Slug = s.Slug;
			this.Title = s.Title;
		}
	}
}