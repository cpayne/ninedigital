﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using nineService;

namespace Testing.nine.christianpayne.com.au
{
	[TestClass]
	public class ProcessorValidationSuccess
	{
		[TestMethod]
		[TestCategory("Success")]
		[DeploymentItem(@"TestData\SampleValid.json", "TestData")]
		public void SuccessTest()
		{
			var input = System.IO.File.ReadAllText(@"TestData\SampleValid.json");

			var service = new ShowService();
			var response = service.Process(input);

			Assert.AreEqual(0, response.Errors.Count());
			Assert.AreEqual(7, response.Shows.Count());
		}
	}
}
