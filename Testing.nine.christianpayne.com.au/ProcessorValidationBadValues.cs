﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using nineService;

namespace Testing.nine.christianpayne.com.au
{
	[TestClass]
	public class ProcessorValidationBadValues
	{
		[TestMethod]
		[TestCategory("Fail")]
		[DeploymentItem(@"TestData\SampleIncorrectTake.json", "TestData")]
		public void SampleInvalidTake()
		{
			var input = System.IO.File.ReadAllText(@"TestData\SampleIncorrectTake.json");

			var service = new ShowService();
			var response = service.Process(input);

			Assert.AreEqual(1, response.Errors.Count());
		}

		[TestMethod]
		[TestCategory("Fail")]
		[DeploymentItem(@"TestData\SampleIncorrectSkip.json", "TestData")]
		public void SampleInvalidSkip()
		{
			var input = System.IO.File.ReadAllText(@"TestData\SampleIncorrectSkip.json");

			var service = new ShowService();
			var response = service.Process(input);

			Assert.AreEqual(1, response.Errors.Count());
		}

		[TestMethod]
		[TestCategory("Fail")]
		[DeploymentItem(@"TestData\SampleIncorrectSkipTakeTooMany.json", "TestData")]
		public void SampleIncorrectSkipTakeTooMany()
		{
			var input = System.IO.File.ReadAllText(@"TestData\SampleIncorrectSkipTakeTooMany.json");

			var service = new ShowService();
			var response = service.Process(input);

			Assert.AreEqual(1, response.Errors.Count());
		}

		[TestMethod]
		[TestCategory("Fail")]
		[DeploymentItem(@"TestData\SampleCorrupt.json", "TestData")]
		public void SampleCorruptFail()
		{
			var input = System.IO.File.ReadAllText(@"TestData\SampleCorrupt.json");

			var service = new ShowService();
			var response = service.Process(input);

			Assert.AreEqual(1, response.Errors.Count());
		}

	}
}
