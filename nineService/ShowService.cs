﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using nineService.Models;

namespace nineService
{
	public class ShowService
	{
		public ShowResponse Process(string data)
		{
			var ret = new ShowResponse();
			if (string.IsNullOrEmpty(data))
			{
				ret.Errors = new string[] { "Input is null" };
				return ret;
			}

			var errors = new List<string>();
			ShowRequest PR = null;
			try
			{
				PR = JsonConvert.DeserializeObject<ShowRequest>(data);
			}
			catch (JsonReaderException)
			{
				// Will throw a JsonReaderException if invalid json is submitted
			}

			if (PR == null)
			{
				ret.Errors = new string[] { "The string submitted isn't a valid ShowRequest object" };
				return ret;
			}

			if (PR.Skip < 0)
			{
				errors.Add("Skip cannot be less than 0");
			}
			if (PR.Take < 0)
			{
				errors.Add("Take cannot be less than 0");
			}

			if (PR.TotalRecords < 0)
			{
				errors.Add("TotalRecords cannot be less than 0");
			}

			if (PR.Skip + PR.Take > PR.Payload.Count() )
			{
				errors.Add("Skip + Take cannot be more than Payload count");
			}

			ret.Errors = errors.ToArray();
			if (ret.Errors.Count() == 0)
			{
				ret.Shows = PR.Payload.Where(p => p.DRM && p.EpisodeCount > 0).ToArray();
			}
			return ret;
		}
	}
}
