﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nineService.Models
{
	public class ShowResponse
	{
		public Show[] Shows;
		public string[] Errors { get; set; }
	}
}
