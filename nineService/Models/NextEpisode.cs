﻿namespace nineService.Models
{
	public class NextEpisode
	{
		public object Channel { get; set; }
		public string ChannelLogo { get; set; }
		public object Date { get; set; }
		public string Html { get; set; }
		public string Url { get; set; }
	}

}