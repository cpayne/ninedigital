﻿using System;
using System.Collections.Generic;
using System.Text;
using nineService.Models;

namespace nineService.Models
{
	public class ShowRequest
	{
		public Show[] Payload { get; set; }
		public int Skip { get; set; }
		public int Take { get; set; }
		public int TotalRecords { get; set; }
	}
}
